use reqwest::blocking::Client;
use serde_json::json;

use crate::error::RequesterError;
use crate::types::{Details, Hosts, LoginInfo, Ports};
use crate::utils::debug_log;

use std::collections::HashMap;

#[derive(Debug)]
pub struct Requester {
    endpoint: Option<String>,
    auth_token: Option<String>,
}

impl Requester {
    pub fn new() -> Requester {
        Requester {
            endpoint: None,
            auth_token: None,
        }
    }

    pub fn set_endpoint(&mut self, endpoint: &str) {
        self.endpoint = Some("https://".to_owned() + endpoint);
    }

    fn apply_auth_header(
        &self,
        request: reqwest::blocking::RequestBuilder,
    ) -> reqwest::blocking::RequestBuilder {
        request.header(
            "Authorization",
            self.auth_token
                .as_ref()
                .expect("Should have been checked already."),
        )
    }

    pub fn login(&mut self, username: &str, password: &str) -> Result<(), RequesterError> {
        if let Some(endpoint) = &self.endpoint {
            let mut post_data = HashMap::new();
            post_data.insert("username", username);
            post_data.insert("password", password);

            let client = Client::new();
            let res = client
                .post(endpoint.to_owned() + "/login")
                .json(&post_data)
                .send();

            match res {
                Ok(res) => match res.json::<LoginInfo>() {
                    Ok(login_info) => {
                        self.auth_token = Some(login_info.token);
                        Ok(())
                    }
                    Err(e) => Err(RequesterError::Parse(e.to_string())),
                },
                Err(_) => Err(RequesterError::Request),
            }
        } else {
            Err(RequesterError::NoEndpoint)
        }
    }

    pub fn hosts(&mut self, count: u32, offset: u32) -> Result<Hosts, RequesterError> {
        if let Some(endpoint) = &self.endpoint {
            if self.auth_token.is_some() {
                let client = Client::new();
                debug_log(&format!("offset: {}", offset));

                let res = self
                    .apply_auth_header(client.post(endpoint.to_owned() + "/hosts"))
                    .json(&json!({
                        "count": count,
                        "offset": offset
                    }))
                    .send();

                match res {
                    Ok(res) => match res.json::<Hosts>() {
                        Ok(hosts) => Ok(hosts),
                        Err(e) => Err(RequesterError::Parse(e.to_string())),
                    },
                    Err(_) => Err(RequesterError::Request),
                }
            } else {
                Err(RequesterError::NoAuth)
            }
        } else {
            Err(RequesterError::NoEndpoint)
        }
    }

    pub fn ports(&mut self, ip: &str) -> Result<Ports, RequesterError> {
        if let Some(endpoint) = &self.endpoint {
            if self.auth_token.is_some() {
                let client = Client::new();
                let res = self
                    .apply_auth_header(client.post(endpoint.to_owned() + "/ports"))
                    .json(&json!({ "ip": ip }))
                    .send();

                match res {
                    Ok(res) => match res.json::<Ports>() {
                        Ok(ports) => Ok(ports),
                        Err(e) => Err(RequesterError::Parse(e.to_string())),
                    },
                    Err(_) => Err(RequesterError::Request),
                }
            } else {
                Err(RequesterError::NoAuth)
            }
        } else {
            Err(RequesterError::NoEndpoint)
        }
    }

    pub fn search(
        &mut self,
        search_term: &str,
        count: u32,
        offset: u32,
    ) -> Result<Hosts, RequesterError> {
        if let Some(endpoint) = &self.endpoint {
            if self.auth_token.is_some() {
                let client = Client::new();
                let res = self
                    .apply_auth_header(client.post(endpoint.to_owned() + "/search"))
                    .json(&json!({
                        "searchTerm": search_term,
                        "count": count,
                        "offset": offset
                    }))
                    .send();

                match res {
                    Ok(res) => match res.json::<Hosts>() {
                        Ok(hosts) => Ok(hosts),
                        Err(e) => Err(RequesterError::Parse(e.to_string())),
                    },
                    Err(_) => Err(RequesterError::Request),
                }
            } else {
                Err(RequesterError::NoAuth)
            }
        } else {
            Err(RequesterError::NoEndpoint)
        }
    }

    pub fn details(&mut self) -> Result<Details, RequesterError> {
        if let Some(endpoint) = &self.endpoint {
            if self.auth_token.is_some() {
                let client = Client::new();

                let res = self
                    .apply_auth_header(client.get(endpoint.to_owned() + "/details"))
                    .send();

                match res {
                    Ok(res) => match res.json::<Details>() {
                        Ok(details) => Ok(details),
                        Err(e) => Err(RequesterError::Parse(e.to_string())),
                    },
                    Err(_) => Err(RequesterError::Request),
                }
            } else {
                Err(RequesterError::NoAuth)
            }
        } else {
            Err(RequesterError::NoEndpoint)
        }
    }
}
