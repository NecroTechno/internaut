use std::fmt;

#[derive(Debug)]
pub enum RequesterError {
    NoEndpoint,
    NoAuth,
    Request,
    Parse(String),
}

impl std::error::Error for RequesterError {}

impl fmt::Display for RequesterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            RequesterError::NoEndpoint => write!(f, "End point not set!"),
            RequesterError::NoAuth => write!(f, "Auth token not set!"),
            RequesterError::Request => write!(f, "Request failed!"),
            RequesterError::Parse(e) => write!(f, "Parse error: {}", e),
        }
    }
}
