use serde::Deserialize;

use chrono::{DateTime, Local};

#[derive(Debug, Deserialize, Clone)]
pub struct Host {
    pub ip: String,
    pub last_updated: DateTime<Local>,
    pub hostname: Option<String>,
    pub protocol: String,
    pub os: String,
    pub os_accuracy: i32,
    pub state: String,
    pub whois: String,
}

#[derive(Debug, Deserialize)]
pub struct Hosts {
    pub hosts: Vec<Host>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Port {
    pub ip: String,
    pub port: i32,
    pub protocol: String,
    pub name: Option<String>,
    pub state: Option<String>,
    pub service: Option<String>,
    pub info: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Ports {
    pub ports: Vec<Port>,
}

#[derive(Debug, Deserialize)]
pub struct LoginInfo {
    pub token: String,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Details {
    pub host_count: u32,
}
