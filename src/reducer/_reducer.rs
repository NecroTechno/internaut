use crate::reducer::state::State;
use crate::reducer::types::{AvailableViews, UpdateInstructions};

pub struct Broadcasters {
    pub view: Vec<Box<dyn FnMut()>>,
}

pub struct Reducer {
    pub state: Option<State<'static>>,
    pub broadcasters: Broadcasters,
}

impl Reducer {
    pub fn new() -> Reducer {
        Reducer {
            state: None,
            broadcasters: Broadcasters { view: Vec::new() },
        }
    }

    pub fn init_state(&mut self) {
        self.state = Some(State::new(move |instructions: Vec<UpdateInstructions>, red: &mut Reducer| {
            instructions.iter().for_each(|i| {
                match i {
                    UpdateInstructions::ChangeView(_) => {
                        for index in 0..red.broadcasters.view.len() {
                            red.broadcasters.view[index]()
                        }
                    }
                };
            })
        }, &mut self))
    }

    pub fn register_cb(&mut self, instruction: UpdateInstructions, c: impl FnMut() + 'static) {
        match instruction {
            UpdateInstructions::ChangeView(_) => self.broadcasters.view.push(Box::new(c)),
        }
    }
}
