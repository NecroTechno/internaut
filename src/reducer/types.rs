use crate::types::{Details, Host, Port};

#[derive(Clone, Debug)]
pub enum AvailableViews {
    Login,
    Home,
    SingleHost,
}

#[derive(Clone)]
pub enum UpdateInstructions {
    ChangeView(AvailableViews),
    UpdateHosts(Option<Vec<Host>>),
    UpdatePorts(Option<Vec<Port>>),
    UpdateCurrentHost(Host),
    UpdateOffset(u32),
    UpdateDetails(Details),
    UpdateSearchStatus(Option<String>),
}
