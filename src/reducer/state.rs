use crate::reducer::types::{AvailableViews, UpdateInstructions};
use crate::types::{Details, Host, Port};

#[derive(Clone)]
pub struct State {
    pub view: AvailableViews,
    pub hosts: Option<Vec<Host>>,
    pub ports: Option<Vec<Port>>,
    pub current_host: Option<Host>,
    pub host_offset: u32,
    pub details: Option<Details>,
    pub searching: Option<String>,
}

impl State {
    pub fn new() -> State {
        State {
            view: AvailableViews::Login,
            hosts: None,
            ports: None,
            current_host: None,
            host_offset: 0,
            details: None,
            searching: None,
        }
    }

    pub fn dispatch(&mut self, instructions: Vec<UpdateInstructions>) {
        instructions.iter().for_each(|i| match i {
            UpdateInstructions::ChangeView(v) => self.view = v.clone(),
            UpdateInstructions::UpdateHosts(h) => self.hosts = h.clone(),
            UpdateInstructions::UpdatePorts(p) => self.ports = p.clone(),
            UpdateInstructions::UpdateCurrentHost(h) => self.current_host = Some(h.clone()),
            UpdateInstructions::UpdateOffset(o) => self.host_offset = *o,
            UpdateInstructions::UpdateDetails(d) => self.details = Some(d.clone()),
            UpdateInstructions::UpdateSearchStatus(s) => self.searching = s.to_owned(),
        });
    }
}
