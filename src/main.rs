#[macro_use]
extern crate lazy_static;

mod error;
mod reducer;
mod requests;
mod types;
mod utils;
mod views;

use crate::reducer::state::State;
use crate::requests::Requester;
use crate::views::render::render;

use cursive::{Cursive, CursiveExt};

use std::sync::Mutex;

// let mut requester = Requester::new();

//     requester.set_endpoint("interopticon.necro.tech");
//     requester.login("necrotechno", "Juyh67juyh67");

//     println!("{:?}", requester.hosts(20, None));
//     println!("{:?}", requester.ports("91.97.220.5"));
//     println!("{:?}", requester.search("ssh", 20, None));

lazy_static! {
    // static ref STATE: State = State::new();
    static ref STATE: Mutex<State> = Mutex::new(State::new());
    static ref REQUESTER: Mutex<Requester> = Mutex::new(Requester::new());
}

const LOGO: &str = " _       _                              _   
(_)     | |                            | |  
 _ _ __ | |_ ___ _ __ _ __   __ _ _   _| |_ 
| | '_ \\| __/ _ \\ '__| '_ \\ / _` | | | | __|
| | | | | ||  __/ |  | | | | (_| | |_| | |_ 
|_|_| |_|\\__\\___|_|  |_| |_|\\__,_|\\__,_|\\__|

";

fn main() {
    let mut siv = Cursive::new();

    println!("{}", LOGO);

    siv.add_global_callback('q', |siv| siv.quit());

    siv.add_global_callback('~', Cursive::toggle_debug_console);

    render(&mut siv, &STATE);

    siv.run();
}
