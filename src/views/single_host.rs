use cursive::align::HAlign;
use cursive::event::Key;

use cursive::views::{OnEventView, PaddedView, Panel, ResizedView, ScrollView, TextView};
use cursive::Cursive;
use cursive::View;

use crate::lock_retr;
use crate::reducer::state::State;
use crate::reducer::types::{AvailableViews, UpdateInstructions};

use crate::types::{Host, Port};
use crate::views::render::render;
use crate::views::utils::info_box;
use crate::REQUESTER;

use std::sync::Mutex;

fn format_host_content(hd: &Host, pv: &Vec<Port>) -> String {
    let mut pi = "".to_string();

    pv.iter().for_each(|p| {
        let mut temp_info = format!(
            "Port: {port}\nProtocol: {protocol}\n",
            port = p.port,
            protocol = p.protocol
        );
        if let Some(name) = &p.name {
            temp_info.push_str(&format!("Name: {}\n", name));
        };
        if let Some(state) = &p.state {
            temp_info.push_str(&format!("State: {}\n", state));
        };
        if let Some(service) = &p.service {
            temp_info.push_str(&format!("Service: {}\n", service));
        };
        if let Some(info) = &p.info {
            temp_info.push_str(&format!("Info: {}\n", info));
        };
        temp_info.push_str(&"\n".to_string());

        pi.push_str(&temp_info);
    });

    let mut host_info = format!(
        "IP: {ip}\nLast Update: {last_updated}\nHostname: {hostname}\nProtocol: {protocol}\nOS: {os} (Accuracy: {os_accuracy})\nState: {state}\n",
        ip = hd.ip,
        last_updated = hd.last_updated,
        hostname = hd.hostname.as_ref().unwrap_or(&"No hostname".to_string()),
        protocol = hd.protocol,
        os = hd.os,
        os_accuracy = hd.os_accuracy,
        state = hd.state
    );

    if !pi.is_empty() {
        host_info.push_str(&format!("\nPort Info\n{}\n", pi));
    };

    host_info.push_str(&format!("Whois:\n{}", hd.whois));

    host_info
}

pub fn single_host(siv: &mut Cursive, app_state: &'static Mutex<State>) {
    let current_host_option = lock_retr!(app_state).current_host.clone();
    if let Some(current_host_details) = current_host_option {
        let current_ports = lock_retr!(app_state).ports.clone();
        if let Some(ports_vec) = current_ports {
            siv.add_fullscreen_layer(
                OnEventView::new(
                    Panel::new(PaddedView::lrtb(
                        2,
                        2,
                        0,
                        0,
                        ResizedView::with_full_screen(Panel::new(ResizedView::with_full_screen(
                            ScrollView::new(TextView::new(format_host_content(
                                &current_host_details,
                                &ports_vec,
                            ))),
                        ))),
                    ))
                    .title(&current_host_details.ip.to_string())
                    .title_position(HAlign::Left),
                )
                .on_event(Key::Left, move |siv| {
                    lock_retr!(app_state).dispatch(vec![
                        UpdateInstructions::UpdatePorts(None),
                        UpdateInstructions::ChangeView(AvailableViews::Home),
                    ]);
                    siv.pop_layer();
                    render(siv, app_state);
                }),
            );
        } else {
            lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdatePorts(Some(
                lock_retr!(REQUESTER)
                    .ports(&current_host_details.ip)
                    .unwrap()
                    .ports,
            ))]);

            render(siv, app_state);
        }
    } else {
        // have a generic error handler function
        siv.add_layer(info_box("error!", true));
    }
}
