use cursive::align::HAlign;
use cursive::view::Nameable;
use cursive::views::{Dialog, EditView, LinearLayout, PaddedView, Panel, ResizedView, TextView};
use cursive::Cursive;

use crate::lock_retr;
use crate::reducer::state::State;
use crate::reducer::types::{AvailableViews, UpdateInstructions};

use crate::views::render::render;
use crate::views::utils::info_box;
use crate::LOGO;
use crate::REQUESTER;

use std::sync::Mutex;

pub fn login(siv: &mut Cursive, app_state: &'static Mutex<State>) {
    let login_screen = ResizedView::with_min_width(
        40,
        LinearLayout::vertical()
            .child(Panel::new(PaddedView::lrtb(
                2,
                2,
                1,
                0,
                TextView::new(LOGO),
            )))
            .child(TextView::new("Server"))
            .child(EditView::new().with_name("server_field"))
            .child(TextView::new("Username"))
            .child(EditView::new().with_name("username_field"))
            .child(TextView::new("Password"))
            .child(EditView::new().secret().with_name("password_field")),
    );

    siv.add_layer(
        Dialog::around(login_screen)
            .title("Login")
            .title_position(HAlign::Left)
            .button("Submit", move |siv| {
                // handle this better than unwraps
                let server = siv
                    .call_on_name("server_field", |view: &mut EditView| view.get_content())
                    .unwrap();
                let username = siv
                    .call_on_name("username_field", |view: &mut EditView| view.get_content())
                    .unwrap();
                let password = siv
                    .call_on_name("password_field", |view: &mut EditView| view.get_content())
                    .unwrap();

                let _ = REQUESTER.lock().unwrap().set_endpoint(&server);
                let login = REQUESTER.lock().unwrap().login(&username, &password);

                match login {
                    Ok(_) => {
                        // s.add_layer(info_box("Success", true));
                        lock_retr!(app_state)
                            .dispatch(vec![UpdateInstructions::ChangeView(AvailableViews::Home)]);
                        siv.pop_layer();
                        render(siv, app_state);
                    }
                    Err(_) => {
                        siv.add_layer(info_box("Fail. Please try again.", true));
                    }
                }
            }),
    );
}
