use cursive::align::HAlign;
use cursive::view::Nameable;
use cursive::views::{Dialog, EditView, LinearLayout, ResizedView, TextView};
use cursive::Cursive;

use crate::lock_retr;
use crate::reducer::state::State;
use crate::reducer::types::UpdateInstructions;

use crate::views::home::search_request;
use crate::views::render::render;

use std::sync::Mutex;

pub fn search(siv: &mut Cursive, app_state: &'static Mutex<State>) {
    let search_dialog = ResizedView::with_min_width(
        40,
        LinearLayout::vertical()
            .child(TextView::new("Query"))
            .child(EditView::new().with_name("search_field")),
    );

    siv.add_layer(
        Dialog::around(search_dialog)
            .title("Search")
            .title_position(HAlign::Left)
            .button("Submit", move |siv| {
                // handle this better than unwraps
                let query = siv
                    .call_on_name("search_field", |view: &mut EditView| view.get_content())
                    .unwrap();

                let _offset = lock_retr!(app_state).host_offset;
                lock_retr!(app_state).dispatch(vec![
                    UpdateInstructions::UpdateOffset(0),
                    UpdateInstructions::UpdateSearchStatus(Some(query.to_string())),
                    UpdateInstructions::UpdateHosts(Some(search_request(
                        &query, app_state, None, 0,
                    ))),
                ]);

                siv.pop_layer();
                siv.pop_layer();
                render(siv, app_state);
            }),
    );
}
