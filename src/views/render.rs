use cursive::Cursive;

use crate::lock_retr;
use crate::reducer::state::State;
use crate::reducer::types::AvailableViews;
use crate::views::{home::home, login::login, single_host::single_host};

use std::sync::Mutex;

pub fn render(siv: &mut Cursive, app_state: &'static Mutex<State>) {
    let current_view = lock_retr!(app_state).view.to_owned();
    match current_view {
        AvailableViews::Login => login(siv, app_state),
        AvailableViews::Home => home(siv, app_state),
        AvailableViews::SingleHost => single_host(siv, app_state),
    }
}
