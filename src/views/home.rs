use cursive::align::HAlign;
use cursive::event::Key;

use cursive::views::{
    LinearLayout, OnEventView, PaddedView, Panel, ResizedView, ScrollView, SelectView, TextView,
};
use cursive::Cursive;
use cursive::View;

use crate::lock_retr;
use crate::reducer::state::State;
use crate::reducer::types::{AvailableViews, UpdateInstructions};

use crate::types::{Details, Host};

use crate::views::render::render;
use crate::views::search::search;

use crate::REQUESTER;

use std::sync::Mutex;

fn hosts_request(_app_state: &'static Mutex<State>, count: Option<u32>, offset: u32) -> Vec<Host> {
    lock_retr!(REQUESTER)
        .hosts(count.unwrap_or(100), offset)
        .unwrap()
        .hosts
}

pub fn search_request(
    query: &str,
    _app_state: &'static Mutex<State>,
    count: Option<u32>,
    offset: u32,
) -> Vec<Host> {
    lock_retr!(REQUESTER)
        .search(query, count.unwrap_or(100), offset)
        .unwrap()
        .hosts
}

enum NavigationType {
    Forward,
    Backward,
}

// should inform user if unable to navigate
fn navigate(app_state: &'static Mutex<State>, navigation_type: NavigationType) {
    let mut offset = lock_retr!(app_state).host_offset;
    let searching = lock_retr!(app_state).searching.clone();

    match navigation_type {
        NavigationType::Forward => {
            offset += 100;

            lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdateOffset(offset)]);
            if let Some(query) = searching {
                lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdateHosts(Some(
                    search_request(&query, app_state, None, offset),
                ))]);
            } else {
                lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdateHosts(Some(
                    hosts_request(app_state, None, offset),
                ))]);
            }
        }
        NavigationType::Backward => {
            if offset > 0 {
                offset -= 100;

                lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdateOffset(offset)]);
                if let Some(query) = searching {
                    lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdateHosts(Some(
                        search_request(&query, app_state, None, offset),
                    ))]);
                } else {
                    lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdateHosts(Some(
                        hosts_request(app_state, None, offset),
                    ))]);
                }
            }
        }
    }
}

fn calculate_pages(offset: u32, db_details: Option<Details>, searching: Option<String>) -> String {
    if searching.is_some() {
        format!("Offset: {}", offset)
    } else {
        match db_details {
            Some(deets) => {
                let cur_page = if offset == 0 { 1 } else { (offset / 100) + 1 };
                let pages_remaining = (deets.host_count as f32 / 100_f32).floor();

                format!("Page {} / {}", cur_page, pages_remaining)
            }
            None => "Unable to calculate pages".to_string(),
        }
    }
}

fn set_selected_host(current_host: Option<Host>, hosts_vec: Vec<Host>) -> usize {
    if let Some(host) = current_host {
        hosts_vec
            .into_iter()
            .position(|h| host.ip == h.ip)
            .unwrap_or(0)
    } else {
        0_usize
    }
}

pub fn home(siv: &mut Cursive, app_state: &'static Mutex<State>) {
    let current_hosts = lock_retr!(app_state).hosts.clone();
    let offset = lock_retr!(app_state).host_offset;
    let db_details = lock_retr!(app_state).details.clone();
    let searching = lock_retr!(app_state).searching.clone();
    let current_host = lock_retr!(app_state).current_host.clone();
    if let Some(hosts_vec) = current_hosts {
        siv.add_fullscreen_layer(
            OnEventView::new(
                Panel::new(PaddedView::lrtb(
                    2,
                    2,
                    0,
                    0,
                    ResizedView::with_full_screen(
                        LinearLayout::vertical()
                            .child(Panel::new(TextView::new(calculate_pages(
                                offset, db_details, searching,
                            ))))
                            .child(Panel::new(ResizedView::with_full_screen(ScrollView::new(
                                SelectView::new()
                                    .h_align(HAlign::Left)
                                    .with_all(
                                        hosts_vec
                                            .to_owned()
                                            .into_iter()
                                            .map(|h| (format!("{} --- {}", h.ip, h.os), h)),
                                    )
                                    .on_submit(move |siv, h| {
                                        lock_retr!(app_state).dispatch(vec![
                                            UpdateInstructions::UpdateCurrentHost(h.to_owned()),
                                        ]);

                                        lock_retr!(app_state).dispatch(vec![
                                            UpdateInstructions::ChangeView(
                                                AvailableViews::SingleHost,
                                            ),
                                        ]);

                                        siv.pop_layer();
                                        render(siv, app_state);
                                    })
                                    .selected(set_selected_host(current_host, hosts_vec)),
                            ))))
                            .child(Panel::new(TextView::new(
                                "Use the left and right arrow keys to browse results.",
                            ))),
                        //.child(Panel::new(Button::new("Ok", |s| s.quit()))),
                    ),
                ))
                .title("Home")
                .title_position(HAlign::Left),
            )
            .on_event(Key::Left, move |siv| {
                navigate(app_state, NavigationType::Backward);
                siv.pop_layer();
                render(siv, app_state);
            })
            .on_event(Key::Right, move |siv| {
                navigate(app_state, NavigationType::Forward);
                siv.pop_layer();
                render(siv, app_state);
            })
            .on_event('s', move |siv| {
                search(siv, app_state);
            })
            .on_event('c', move |siv| {
                let searching = lock_retr!(app_state).searching.clone();
                if searching.is_some() {
                    lock_retr!(app_state).dispatch(vec![
                        UpdateInstructions::UpdateOffset(0),
                        UpdateInstructions::UpdateSearchStatus(None),
                        UpdateInstructions::UpdateHosts(None),
                    ]);
                    render(siv, app_state);
                }
            }),
        );
    } else {
        // should be moved into it's own setup function

        // DEBUG
        // lock_retr!(REQUESTER).set_endpoint(&env::var("SERVER_URL").expect("SERVER_URL VAR NOT SET"));
        // lock_retr!(REQUESTER).login(
        //     &env::var("SERVER_USERNAME").expect("SERVER_URL VAR NOT SET"),
        //     &env::var("SERVER_PASSWORD").expect("SERVER_PASSWORD VAR NOT SET"),
        // );

        lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdateDetails(
            lock_retr!(REQUESTER).details().unwrap(),
        )]);

        lock_retr!(app_state).dispatch(vec![UpdateInstructions::UpdateHosts(Some(hosts_request(
            app_state, None, 0,
        )))]);

        render(siv, app_state);
    }
}
