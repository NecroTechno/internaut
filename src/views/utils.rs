use cursive::align::HAlign;
use cursive::views::{Dialog, PaddedView, TextView};

pub fn info_box(dialog_text: &str, dismissible: bool) -> Dialog {
    let mut info_box = Dialog::around(PaddedView::lrtb(2, 2, 1, 0, TextView::new(dialog_text)))
        .title("Info")
        .title_position(HAlign::Left);

    if dismissible {
        info_box = info_box.dismiss_button("Close");
    }

    info_box
}
