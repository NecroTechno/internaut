use cursive::logger::log;
use log::{Level, Record};

pub fn debug_log(log_info: &str) {
    log(&Record::builder()
        .args(format_args!("{}", log_info))
        .level(Level::Debug)
        .build());
}

#[macro_export]
macro_rules! lock_retr {
    ( $locked:ident ) => {
        $locked.lock().unwrap()
    };
}
